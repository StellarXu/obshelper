import numpy as np
import matplotlib.pyplot as plt
import hiviewer as hv
import os
from astropy import log

from astropy import coordinates as coords
import astropy.units as u

class MultiOTFcalculator(object):
    def __init__(self, ra, dec, dist = None,diff_ra = None, diff_dec = None,start = None, end = None):
        self.ra = ra
        self.dec = dec
        self.dist = dist
        if diff_ra is not None and diff_dec is not None:
            self.diff_ra = diff_ra * u.arcmin
            self.diff_dec = diff_dec * u.arcmin
        self.start = start
        self.end = end
    @property
    def center(self):
        return coords.SkyCoord(ra = self.ra * u.degree, dec = self.dec * u.degree, 
                               distance = self.dist * u.Mpc, frame='icrs')
    
    def get_startend(self):
        if self.start is None or self.end is None:
            self.start = coords.SkyCoord(ra = self.center.ra + self.diff_ra/2, dec = self.center.dec + self.diff_dec/2, frame='icrs')
            self.end = coords.SkyCoord(ra = self.center.ra - self.diff_ra/2, dec = self.center.dec - self.diff_dec/2, frame='icrs')
        else:
            self.start = coords.SkyCoord(self.start, unit=(u.hourangle, u.deg), frame='icrs')
            self.end = coords.SkyCoord(self.end, unit=(u.hourangle, u.deg), frame='icrs')
            
            self.diff_ra = np.abs(self.start.ra - self.end.ra).to(u.arcmin)
            self.diff_dec = np.abs(self.start.dec - self.end.dec).to(u.arcmin)
        
    def input_OTF_para(self, direction = '-', scan_gap = 21.66, scan_speed = 15):
        self.direction = direction
        self.scan_gap = scan_gap * u.arcmin
        self.scan_speed = scan_speed * u.arcsec / u.second
        
        if direction == '-':
            self.switch_time = 90 * u.second
        elif direction == '|':
            self.switch_time = 54 * u.second
            
        print("MultiBeam OTF, direction: ", self.direction)
        print("Scan gap:", self.scan_gap)
        print("Scan speed:", self.scan_speed)
            
    def calculate_time(self):
        
        if self.direction == '-':
            '''
            formula = diffRA / float(speed) * round(diffDec / float(gap) / 60.0 + 1.0) + switchTime * round(
                diffDec / float(gap) / 60.0)
            '''
            # 2021.08.13
            # Round to the nearest whole number, because it needs to be divisible by 0.2 and 0.5
            self.onePathTime = np.round((self.diff_ra / self.scan_speed).to(u.second))
            self.scanTimes = np.round(self.diff_dec / self.scan_gap + 1.0)
            self.switchTimes = np.round(self.diff_dec / self.scan_gap)
        elif self.direction == '|':
            '''
            formula = diffDec / float(speed) * round(diffRA / float(gap) / 60.0 + 1.0) + switchTime * round(
            diffRA / float(gap) / 60.0)
            '''
            # 2021.08.13
            # Round to the nearest whole number, because it needs to be divisible by 0.2 and 0.5
            self.onePathTime = np.round((self.diff_ra / self.scan_speed).to(u.second))
            self.scanTimes = np.round(self.diff_ra / self.scan_gap + 1.0)
            self.switchTimes = np.round(self.diff_ra / self.scan_gap)
            
        self.tot_time = self.onePathTime * self.scanTimes + self.switch_time * self.switchTimes
        print(f"Scan {self.scanTimes} times along {self.direction}, switch {self.switchTimes} times.")
        print(f"Need total {self.tot_time} = {self.tot_time.to(u.minute)}.")
        
    def footprints_M01(self, sample_time, head = -1):
        self.sample_time = sample_time * u.second
        print("Sample time:", self.sample_time)
        
        onePathPoints = self.onePathTime / self.sample_time
        onePathStep = self.onePathTime / onePathPoints * self.scan_speed
        if self.direction == '-':
            ra1 = self.start.ra + head * np.arange(0, onePathStep.value * (onePathPoints + 1), onePathStep.value) * onePathStep.unit 
            dec1 = self.start.dec + head * np.arange(0, self.scan_gap.value * (self.switchTimes + 1), self.scan_gap.value) * self.scan_gap.unit
        elif self.direction == '|':
            dec1 = self.start.dec + head * np.arange(0, onePathStep.value * (onePathPoints + 1), onePathStep.value) * onePathStep.unit 
            ra1 = self.start.ra + head * np.arange(0, self.scan_gap.value * (self.switchTimes + 1), self.scan_gap.value) * self.scan_gap.unit
        
        self.mesh1 = np.meshgrid(ra1.value, dec1.value)
        
        self.ra1 = self.mesh1[0].flatten()
        self.dec1 = self.mesh1[1].flatten()
        
    def footprints_all(self, filename = None):
        if self.direction == '-':
            filename = '/data/inspur_disk01/userdir/xuc/FAST/test_fast/backend/data/beam_position.npz'
        beams = np.load(filename)
        
        ras = beams['ra']
        decs = beams['dec']
        
        self.ra_all = np.full(((19,len(self.ra1))), self.ra1) + np.full((len(self.ra1),19),ras).T
        self.dec_all = np.full(((19,len(self.dec1))), self.dec1) + np.full((len(self.dec1),19),decs).T
        
    def show_footprints(self, ax, opt, **kwargs):
        self.footprints_M01(**kwargs)
        self.footprints_all()
        
#         fig, ax = plt.subplots(figsize = (5,5))
        for i in range(19):
            c = 'r' if i == 0 else 'C0'
            px, py = opt.deg2pix(self.ra_all[i], self.dec_all[i],around = False)
            ax.plot(px, py, '.', c = c, ms = 1)
        ax.set_xlabel('ra')    
        ax.set_ylabel('dec')  
        
    def plot_radius(self, ax, opt, radius, dr = 1):
        r = radius * u.arcmin
        center = self.center        
        ra = self.ra_all.flatten()
        dec = self.dec_all.flatten()
        dr = dr * u.arcmin
        
        c = coords.SkyCoord(ra = ra * u.degree, dec = dec * u.degree, 
                                frame='icrs')
        sep = c.separation(center).to(u.arcmin)
        
        use = (sep > r - dr) & (sep < r + dr)
            
        px, py = opt.deg2pix(ra[use], dec[use],around = False)
        ax.plot(px, py, '.', c = 'C1', ms = 1)
        
