#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import ephem as ep
from astropy.time import Time
from astropy import units as u
from astropy.coordinates import Angle

def bj_time(s):
    t = Time(str(s).replace('/','-'))
    return t + 8*u.hour


def line_set(ax,xlabel,ylabel,direction='in',xlim=None,ylim=None,legend=True,
             title=None,loc='best', size = None,frameon=True):
    """
    plot settings
    """
    if size is None:
        size = {'mz': 1,   # Set thickness of the tick marks
                'lz': 3,   # Set length of the tick marks
                'lbz': 14,  # Set label size
                'tkz': 12,  # Set tick size
                }
    mz = size['mz']; lz = size['lz']
    lbz = size['lbz']; tkz = size['tkz']
    # Make tick lines thicker
    for l in ax.get_xticklines():
        l.set_markersize(lz)
        l.set_markeredgewidth(mz)
    for l in ax.get_yticklines():
        l.set_markersize(lz)
        l.set_markeredgewidth(mz)

    # Make figure box thicker
    for s in ax.spines.values():
        s.set_linewidth(mz)
    ax.minorticks_on()
    ax.tick_params("both",which = 'both',direction=direction,labelsize=tkz,
                  bottom=True, top=True,left=True,right=True)
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    ax.set_xlabel(xlabel,fontsize=lbz)
    ax.set_ylabel(ylabel,fontsize=lbz)
    if legend: ax.legend(loc = loc,fontsize=tkz, frameon=frameon)
    if title is not None: ax.set_title(title,fontsize=lbz)

def plot_za(BJ_time, coord):
    
    t0 = Time(BJ_time)
    ts = t0 + np.arange(-12, 12, 0.1)*u.hour

    obj = ep.FixedBody()
    obj._ra, obj._dec = coord
    obj._epoch = '2000'

    zas = []
    for i in range(len(ts)):
        t = ts[i]
        ut = t - 8*u.hour

        fast = ep.Observer()
        fast.lon = '106.8566667'    # East positive, deg
        fast.lat = '25.65294444'    # North positive, deg
        fast.elevation = 1110.0288      # Altitude, m 
        fast.date = ut.value

        obj.compute(fast)

        alt = Angle(str(obj.alt),unit = u.deg)
        za = 90*u.deg - alt
        # az = Angle(str(obj.az),unit = u.deg)
        # print(t)
        # print("Current Altitude:", alt, "Zenith Angle", za, "Azimuth angle:", az)
        zas += [za.value,]
        
        if np.abs((t-t0).to_value(u.hour)) < 0.01:
            print("#### Object ####")
            print("Previous Transit time:",bj_time(fast.previous_transit(obj)))
            print("Next Transit time:",bj_time(fast.next_transit(obj)))
            print("#### Sun ####")
            sun_rise = bj_time(fast.next_rising(ep.Sun()))
            sun_set = bj_time(fast.previous_setting(ep.Sun()))
            # at night
            if np.abs((sun_rise - t0).to_value(u.hour)) < 12:
                print("Next Rise time:",sun_rise)
                print("Previous Set time:",sun_set)
            else:
                sun_rise = bj_time(fast.previous_rising(ep.Sun()))
                sun_set = bj_time(fast.next_setting(ep.Sun()))
                print("Previous Rise time:",sun_rise)
                print("Next Set time:",sun_set)
    zas = np.array(zas)

    unrise = (zas > 90)
    zas[unrise] = np.nan

    fig, ax = plt.subplots(figsize = (10,5))
    ax.plot_date(ts.plot_date, zas, fmt = '.')  
    plt.gcf().autofmt_xdate()  # orient date labels at a slant  
    [ax.axhline(y = ii,c='C1',ls='--') for ii in [20,30,40]]
    ax.invert_yaxis()
    ax.grid()
    
    if sun_rise > sun_set:
        s1 = sun_set
        s2 = sun_rise
        at_night = True
    else:
        s1 = sun_rise
        s2 = sun_set
        at_night = False
    
    print("input time at night?:", at_night)
    
    if at_night:  
        ax.fill_betweenx(y = [np.nanmin(zas),np.nanmax(zas)], x1 = s1.plot_date,x2 = s2.plot_date,alpha=.1,label='night',color = 'k')
    else:
        ax.fill_betweenx(y = [np.nanmin(zas),np.nanmax(zas)], x1 = ts[0].plot_date,x2 = s1.plot_date,alpha=.1,label='night',color = 'k')
        ax.fill_betweenx(y = [np.nanmin(zas),np.nanmax(zas)], x1 = s2.plot_date,x2 = ts[-1].plot_date,alpha=.1,color = 'k')
    line_set(ax, xlabel = 'time (date-hour)', ylabel = 'ZA (deg)',)
    plt.show()
    
    