import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import hiviewer as hv
import os
from astropy import log

from astroquery.skyview import SkyView
from astropy import coordinates as coords
import astropy.units as u
from astropy.table import Table

class OptSource(object):
    def __init__(self, name, ra, dec, dist):
        self.name = name
        self.ra = float(ra)
        self.dec = float(dec)
        self.dist = dist
        self.get_pos()
    
    @property
    def center(self):
        return coords.SkyCoord(ra = self.ra * u.degree, dec = self.dec * u.degree, 
                               distance = self.dist * u.Mpc, frame='icrs')
    
    def get_pos(self):  
        pos = self.center
        self.pos = pos.to_string()
        self.pos_hmsdms = pos.to_string('hmsdms')
        
    def get_opt_image(self, surveys = ["DSS2 Blue", "SDSSdr7g"], size = 25,write = True):
        """
        https://github.com/AusSRC/SoFiAX
        https://astroquery.readthedocs.io/en/latest/skyview/skyview.html
        """
        self.surveys = surveys
        if write:
            sv = SkyView()
            print("Retrieving...")
            paths = sv.get_images(
                    position=f"{self.ra}, {self.dec}",
                    survey=surveys,
                    coordinates='J2000',
#                     projection = 'TAN',
                    width=size*u.arcmin,
                    height=size*u.arcmin,
                    cache = False)
            
            optnames = []
            for j in range(len(surveys)):
                for i in range(len(paths[j])):
                    hdu_opt = paths[j][i]
                    if not os.path.exists('./tmp'):
                        os.mkdir('./tmp')
                    optname = './tmp/' + self.name + '_' +surveys[j] +f'_{i}-opt.fits'
                    hdu_opt.writeto(optname, overwrite = True)
                    print(f"Saved {optname}")
                    optnames += [optname,]
        else:
            from glob import glob
            paths = glob('./tmp/' + self.name + f'_*-opt.fits')
            if len(paths) > 0:
                print("Loading...")
                optnames = paths
            else:
                raise ValueError("Cannot find opt files in tmp, try 'write = True'")
        print("Finish!")
        self.optnames = optnames    
        
    def show_img(self,survey_index = 0,**kwargs):
        j = survey_index
        bk = hv.FitsPic(self.optnames[j])
        ax = bk.plot_slice(**kwargs)
        return ax
    
    def plot_radius(self, ax, opt, radius, dr = 1, dp = 1):
        r = radius * u.arcmin
        center = self.center  
        ny, nx = opt.data.shape
        py, px = np.meshgrid(np.arange(0, ny, dp), np.arange(0, nx, dp))

        ra, dec = opt.pix2deg(px.flatten(), py.flatten())
        dr = dr * u.arcmin

        c = coords.SkyCoord(ra = ra * u.degree, dec = dec * u.degree, 
                                frame='icrs')
        sep = c.separation(center).to(u.arcmin)

        use = (sep > r - dr) & (sep < r + dr)

        px, py = opt.deg2pix(ra[use], dec[use],around = False)
        ax.plot(px, py, '.', c = 'C1', ms = 1)
    
    def plot_ZA(self, BJ_time = '2022-08-15 03:00:00'):

        from calculate_ZA import plot_za

        coord = self.pos_hmsdms.split(' ')
        for s in ['h','m','s','d']:
            coord[0] = coord[0].replace(s,' ') 
            coord[1] = coord[1].replace(s,' ') 
        plot_za(BJ_time, coord)
        
    def add_table(self, tablename, r = None, keys = ['_RAJ2000','_DEJ2000']):
        tab = Table.read(tablename)
        if r is not None:
            c = coords.SkyCoord(ra = tab[keys[0]], dec = tab[keys[1]] , frame='icrs')
            use = (self.center.separation(c) < r * u.arcmin)
            
            tab = tab[use]

        if 'Object Name' in tab.keys():
            import re
            name = tab['Object Name']
            for n in range(len(name)):
                tab['Object Name'][n] = re.findall(">.*<",name[n])[0][1:-1]

        self.table = tab
        
        
    def ialadin(self, fov = 5/60, survey= 'DSS2+color',width='100%'):
        """
        https://github.com/cds-astro/ipyaladin/blob/master/
        
        Cell as dialog
        https://github.com/innovationOUtside/nb_cell_dialog
        """
        import ipyaladin as ipyal
        from ipywidgets import Layout, Box, widgets

        self.aladin = ipyal.Aladin(target=self.pos, fov = fov, survey = survey,layout = Layout(width=width))
        
        return self.aladin
    
    def get_sep(self):
        table = self.table

        c1 = coords.SkyCoord(ra = table[0]['RA'] * u.degree, dec = table[0]['DEC'] * u.degree, frame='icrs')
        c2 = coords.SkyCoord(ra = table['RA'] * u.degree, dec = table['DEC'] * u.degree, frame='icrs')
        v1 = table[0]['Velocity'] * u.km / u.s
        v2 = table['Velocity'] * u.km / u.s

        table.add_column(v1 - v2,name = 'vel_sep', index = 5)
        table.add_column(c1.separation(c2).to(u.arcmin), name = 'cood_sep', index = 2)
