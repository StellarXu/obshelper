#!/usr/bin/env python
# coding: utf-8

def toArcSec(string):
    arr = string.split()
    result = []
    for item in arr:
        arcSec = 0
        a, b, c = item.split(':')
        arcSec += abs(int(a)) * 3600 + int(b) * 60 + float(c)
        if item.startswith('-'):
            arcSec *= -1.0
        result.append(arcSec)
    return 15.0 * result[0], result[1]

def cal_OTF_time():
    obsMode = input('1 for OnTheFlyMapping, 2 for MultiBeamOTF, you are calculating:')
    while obsMode != '1' and obsMode != '2':
        obsMode = input('Error! 1 for OnTheFlyMapping, 2 for MultiBeamOTF:')
    answer = input('Please input startRA and starDec, e.g.: 01:23:45.6 +65:43:21.0:\n')
    startRA, startDec = toArcSec(answer)
    answer = input('Please input endRA and endDec, e.g.: 01:23:45.6 +65:43:21.0:\n')
    endRA, endDec = toArcSec(answer)

    diffRA = abs(startRA - endRA)
    if diffRA > 648000:
        diffRA = 1296000 - diffRA
    diffDec = abs(startDec - endDec)

    answer = input('Please input scan direction (- or |), scan gap(arcmin) and scan speed(arcsec/s), e.g.:- 5.11 15\n')
    direction, gap, speed = answer.split()

    if direction == '|':
        if obsMode == '1':
            # switchTime = 12.0
            switchTime = 12.0*float(gap)
        else:
            switchTime = 54.0
        '''
        formula = diffDec / float(speed) * round(diffRA / float(gap) / 60.0 + 1.0) + switchTime * round(
            diffRA / float(gap) / 60.0)
        '''
        # 2021.08.13
        # Round to the nearest whole number, because it needs to be divisible by 0.2 and 0.5
        onePathTime = round(diffDec / float(speed))
        formula = onePathTime * round(diffRA / float(gap) / 60.0 + 1.0) + switchTime * round(
            diffRA / float(gap) / 60.0)
    else:
        if obsMode == '1':
            switchTime = 18.0
        else:
            switchTime = 90.0

        '''
        formula = diffRA / float(speed) * round(diffDec / float(gap) / 60.0 + 1.0) + switchTime * round(
            diffDec / float(gap) / 60.0)
        '''
        # 2021.08.13
        # Round to the nearest whole number, because it needs to be divisible by 0.2 and 0.5
        onePathTime = round(diffRA / float(speed))
        formula = onePathTime * round(diffDec / float(gap) / 60.0 + 1.0) + switchTime * round(
            diffDec / float(gap) / 60.0)

    print(formula)
