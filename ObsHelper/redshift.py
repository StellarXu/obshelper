#!/usr/bin/env python
# coding: utf-8
import numpy as np
from astropy import  log

restfreq=1420.405751#7667
c = 299792.458

def vopt2vrad(vopt):
    """
    velocity: optical to radio
    """
    vrad = c -c**2/(c + vopt)
    return vrad

def vrad2vopt(vrad):
    """
    velocity: radio to optical
    """
    vopt = c**2/(c - vrad) - c
    return vopt

def vopt2freq(vopt):
    """
    optical velocity to frequency
    """
    freq = restfreq / (vopt / c + 1)
    
    return freq

def freq2vopt(freq):
    """
    frequency to optical velocity
    """
    vopt = c * (restfreq - freq) / freq
    
    return vopt

def redshift(v , relative = False):
    beta = v / c
    
    if relative:
        g = 1 / np.sqrt(1 - beta ** 2)
        z = (1 + v / c) * g - 1
        return z
    else:
        return beta
