# ObsHelper
FAST 19-beam observation plan helper.

## Description
帮助规划OTF扫描时间，检查目标附近的光学对应体，估计on-off模式观测区域，计算某天某时刻观测该天体的天顶角...

## 需要的Python包：

Python3环境

numpy, matplotlib, astropy, ephem, ipyaladin, ipywidgets

